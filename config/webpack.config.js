// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");

const createBuildTemplate = require("template");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

module.exports = createBuildTemplate(resolveAppPath, {
  module: {
    rules: [
      {
        test: /\.(webp|ogg)$/,
        use: "file-loader",
      },
    ],
  },
  plugins: [new NodePolyfillPlugin()],
});
