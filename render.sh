#!/usr/bin/env bash

echo 'This script requires blender and imagemagick to run.'

for model in ./src/assets/models/*.blend; do blender $model -b --python-text "Render Script"; done

mogrify -format webp ./src/assets/renders/**/**.png
rm ./src/assets/renders/**.png