import React from "react";

import {
  ReverseSwitchContainer,
  ReverseSwitchLabel,
  ReverseSwitchImage,
} from "assets/styles/Controls/ReverseSwitch.styles.jsx";

import sizes from "assets/sizes.json";

const size = sizes.reverseSwitch;

const rotations = Array.from(
  { length: 2 },
  () => `rotate(${Math.random() * 20 - 10}deg)`
);

const ReverseSwitch = ({ height, width, value, changeValue }) => {
  const clickHandler = (e) => {
    e.preventDefault();

    changeValue();
  };

  return (
    <ReverseSwitchContainer style={{ width }}>
      <ReverseSwitchLabel style={{ transform: rotations[0] }}>
        DECRYPT
      </ReverseSwitchLabel>
      <ReverseSwitchImage
        style={{ height }}
        src={require(`assets/renders/reverseSwitch/${value}.webp`).default}
        onMouseDown={(e) => clickHandler(e)}
      />
      <ReverseSwitchLabel style={{ transform: rotations[1] }}>
        ENCRYPT
      </ReverseSwitchLabel>
    </ReverseSwitchContainer>
  );
};

ReverseSwitch.defaultProps = {
  height: size.height,
  width: size.width,
};

export default ReverseSwitch;
