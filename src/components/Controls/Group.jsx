import React from "react";

import { GroupContainer } from "assets/styles/Controls/Group.styles.jsx";

import Knob from "components/Controls/Knob.jsx";
import Slider from "components/Controls/Slider.jsx";

const MAX_SELECTOR_VALUES = 3;

const Group = ({ initialValue, setKey, index = 0 }) => {
  const setKnobKey = (e) =>
    setKey((key) => [
      ...key.slice(0, index),
      [e, key[index][1]].join(""),
      ...key.slice(index + 1),
    ]);
  const setSliderKey = (e) =>
    setKey((key) => [
      ...key.slice(0, index),
      [key[index][0], e].join(""),
      ...key.slice(index + 1),
    ]);

  return (
    <GroupContainer>
      <Knob
        min={0}
        max={MAX_SELECTOR_VALUES}
        initialValue={"ACGT".indexOf(initialValue[0])}
        index={index}
        setKey={setKnobKey}
      />
      <Slider
        min={0}
        max={MAX_SELECTOR_VALUES}
        initialValue={"ACGT".indexOf(initialValue[1])}
        index={index}
        setKey={setSliderKey}
      />
    </GroupContainer>
  );
};

export default Group;
