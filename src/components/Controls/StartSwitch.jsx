import React from "react";

import { StartSwitchImage } from "assets/styles/Controls/StartSwitch.styles.jsx";

import sizes from "assets/sizes.json";

const size = sizes.startSwitch;

const StartSwitch = ({ height, width, value, changeValue }) => {
  const clickHandler = (e) => {
    e.preventDefault();

    changeValue();
  };

  return (
    <StartSwitchImage
      style={{ height, width }}
      src={require(`assets/renders/startSwitch/${value}.webp`).default}
      onMouseDown={(e) => clickHandler(e)}
    />
  );
};

StartSwitch.defaultProps = {
  height: size.height,
  width: size.width,
};

export default StartSwitch;
