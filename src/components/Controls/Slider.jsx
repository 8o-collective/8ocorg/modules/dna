import React, { useState, useRef } from "react";

import {
  SliderContainer,
  SliderTicks,
  SliderTick,
  SliderImage,
  tickHeight,
} from "assets/styles/Controls/Slider.styles.jsx";

import sizes from "assets/sizes.json";

const size = sizes.slider;

const tickValues = ["A", "C", "G", "T"];

const convertRange = (oldMin, oldMax, newMin, newMax, oldValue) =>
  ((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin) +
  newMin +
  0.001;

// https://stackoverflow.com/a/13635455/13584955
const nearest = (value, min, max, steps) => {
  let zerone = Math.round(((value - min) * steps) / (max - min)) / steps; // bring to 0-1 range
  zerone = Math.min(Math.max(zerone, 0), 1); // keep in range in case value is off limits
  return zerone * (max - min) + min;
};

const Slider = ({ height, min, max, initialValue, index, setKey }) => {
  const numTicks = max - min;

  const width = height / 5;
  const margin = width * 1.15;

  const startDistance = height / 2;
  const endDistance = -height / 2;
  const fullDistance = startDistance - endDistance;

  const imperfections = useRef({
    rotations: Array.from(
      { length: numTicks + 1 },
      () => `rotate(${Math.random() * 20 - 10}deg)`
    ),
    margins: Array.from(
      { length: numTicks + 1 },
      () => `${Math.random() * 6}px 0px`
    ),
    paddings: Array.from(
      { length: numTicks + 1 },
      () => `0px ${Math.random() * 3}px`
    ),
  });

  // const [currentDistance, setCurrentDistance] = useState(
  //   convertRange(startDistance, endDistance, min, max, initialValue)
  // );
  const [value, setValue] = useState(initialValue);

  // useEffect(() => setKey(['A', 'C', 'G', 'T'][initialValue]), [])

  const getDistance = (cY, pts) => {
    return nearest(cY - pts.y, startDistance, endDistance, numTicks);
  };

  const startDrag = (e) => {
    e.preventDefault();

    const slider = e.target.getBoundingClientRect();
    const pts = {
      y: slider.top + slider.height / 2,
    };

    const moveHandler = (e) => {
      let distance = getDistance(e.clientY, pts);

      // setCurrentDistance(distance);

      let newValue = Math.floor(
        convertRange(startDistance, endDistance, min, max, distance)
      );

      setValue(newValue);

      if (setKey) {
        setKey(["A", "C", "G", "T"][newValue]); // Pass function to evaluate new value
      }
    };

    document.addEventListener("mousemove", moveHandler);
    document.addEventListener("mouseup", () =>
      document.removeEventListener("mousemove", moveHandler)
    );
  };

  const renderTicks = () => {
    let ticks = [];

    const incr = (fullDistance - tickHeight) / numTicks;

    // Wish I didn't have to do the [] thing here. not declarative. not Lindy >:(
    for (
      let distance = endDistance;
      distance <= startDistance;
      distance += incr
    ) {
      const tick = {
        distance: distance,
        tickStyle: {
          height: tickHeight,
          transform:
            `translate(${margin}px, ${distance + height / 2}px)` +
            imperfections.current.rotations[ticks.length],
          margin: imperfections.current.margins[ticks.length],
          padding: imperfections.current.paddings[ticks.length],
        },
      };
      ticks = [...ticks, tick];
    }

    return ticks;
  };

  return (
    <SliderContainer>
      <SliderTicks>
        {numTicks &&
          renderTicks().map((tick, i) => (
            <SliderTick
              key={i}
              height={tick.tickStyle.height}
              style={tick.tickStyle}
            >
              {tickValues[tickValues.length - i - 1]}
            </SliderTick>
          ))}
      </SliderTicks>
      <SliderImage
        style={{ height, width, margin: `0px ${margin}` }}
        onMouseDown={startDrag}
        src={
          require(`../../assets/renders/slider/loc${index}_pos${value}.webp`)
            .default
        }
      />
    </SliderContainer>
  );
};

Slider.defaultProps = {
  height: size.height,
  min: 0,
  max: 3,
  initialValue: 0,
  index: 0,
};

export default Slider;
