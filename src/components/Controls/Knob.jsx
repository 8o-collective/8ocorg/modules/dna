import React, { useState, useRef } from "react";

import {
  KnobContainer,
  KnobTicks,
  KnobTick,
  KnobImage,
} from "assets/styles/Controls/Knob.styles.jsx";

import sizes from "assets/sizes.json";

const size = sizes.knob;

const tickValues = ["A", "C", "G", "T"];

const convertRange = (oldMin, oldMax, newMin, newMax, oldValue) =>
  ((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin) + newMin; // ?

// https://stackoverflow.com/a/13635455/13584955
const nearest = (value, min, max, steps) => {
  let zerone = Math.round(((value - min) * steps) / (max - min)) / steps; // bring to 0-1 range
  zerone = Math.min(Math.max(zerone, 0), 1); // keep in range in case value is off limits
  return zerone * (max - min) + min;
};

// Adapted from https://codepen.io/bbx/pen/QBKYOy
const Knob = ({ diameter, min, max, degrees, initialValue, index, setKey }) => {
  const fullAngle = degrees;
  const startAngle = (360 - degrees) / 2;
  const endAngle = startAngle + degrees;
  const margin = diameter * 0.3; // ?
  const numTicks = max - min;

  const imperfections = useRef({
    rotations: Array.from(
      { length: numTicks + 1 },
      () => `rotate(${Math.random() * 20 - 10}deg)`
    ),
    margins: Array.from(
      { length: numTicks + 1 },
      () => `${Math.random() * 6}px`
    ),
    paddings: Array.from(
      { length: numTicks + 1 },
      () => `0px ${Math.random() * 5 + 2}px`
    ),
  });

  const baseStyle = {
    width: diameter,
    height: diameter,
    margin: margin,
  };

  // const [currentDegrees, setCurrentDegrees] = useState(
  //   Math.floor(convertRange(min, max, startAngle, endAngle, initialValue))
  // );
  const [value, setValue] = useState(initialValue);

  // useEffect(() => setKey(['A', 'C', 'G', 'T'][initialValue]), [])

  const getDegrees = (cX, cY, pts) => {
    const x = cX - pts.x;
    const y = cY - pts.y;
    let degrees = (Math.atan(y / x) * 180) / Math.PI;
    if ((x < 0 && y >= 0) || (x < 0 && y < 0)) {
      degrees += 90;
    } else {
      degrees += 270;
    }
    const angle = Math.min(Math.max(startAngle, degrees), endAngle);
    return nearest(angle, startAngle, endAngle, numTicks);
  };

  const startDrag = (e) => {
    e.preventDefault();

    const knob = e.target.getBoundingClientRect();
    const pts = {
      x: knob.left + knob.width / 2,
      y: knob.top + knob.height / 2,
    };

    const moveHandler = (e) => {
      let degrees = getDegrees(e.clientX, e.clientY, pts);
      // if (degrees === startAngle) degrees-- // ???

      // setCurrentDegrees(degrees);

      let newValue = Math.floor(
        convertRange(startAngle, endAngle, min, max, degrees)
      );

      setValue(newValue);

      if (setKey) {
        setKey(["A", "C", "G", "T"][newValue]); // Pass function to evaluate new value
      }
    };

    document.addEventListener("mousemove", moveHandler);
    document.addEventListener("mouseup", () =>
      document.removeEventListener("mousemove", moveHandler)
    );
  };

  const renderTicks = () => {
    let ticks = [];

    const incr = fullAngle / numTicks;
    const size = margin + diameter / 2;
    const offset = margin / 2 + diameter / 2;

    // Wish I didn't have to do the [] thing here. not declarative. not Lindy >:(
    for (let degrees = startAngle; degrees <= endAngle; degrees += incr) {
      let radians = degrees * (Math.PI / 180);
      const tick = {
        degrees: degrees,
        tickStyle: {
          transform:
            `translate(${Math.sin(radians) * size + offset}px, ${
              Math.cos(radians) * size + offset
            }px)` + imperfections.current.rotations[ticks.length],
          margin: imperfections.current.margins[ticks.length],
          padding: imperfections.current.paddings[ticks.length],
        },
      };
      ticks = [...ticks, tick];
    }

    return ticks;
  };

  return (
    <KnobContainer>
      <KnobTicks>
        {numTicks &&
          renderTicks().map((tick, i) => (
            <KnobTick key={i} style={tick.tickStyle}>
              {tickValues[tickValues.length - i - 1]}
            </KnobTick>
          ))}
      </KnobTicks>
      <KnobImage
        style={baseStyle}
        onMouseDown={startDrag}
        src={
          require(`../../assets/renders/knob/loc${index}_pos${value}.webp`)
            .default
        }
      />
    </KnobContainer>
  );
};

Knob.defaultProps = {
  diameter: size.height,
  min: 0,
  max: 10,
  degrees: 270,
  initialValue: 0,
  index: 0,
};

export default Knob;
