import React, { useState, useEffect, useRef } from "react";

import randy from "randy";

import {
  DNAContainer,
  DNACanvasContainer,
  DNACanvas,
  DNAInput,
  DNAOutput,
  DNACipherKey,
  DNAHazardLine,
} from "assets/styles/DNA.styles.jsx";

import { DNAAudioManager } from "actions/dna.sounds.js";

import ReverseSwitch from "components/Controls/ReverseSwitch.jsx";
import StartSwitch from "components/Controls/StartSwitch.jsx";

import frameRenderer from "actions/dna.renderer.js";

import { encrypt, decrypt, cyrb53, CYCLES } from "actions/dna.cipher.js";

const CONNECTORS = 3;
const TOTAL_CONNECTORS = 2 * CONNECTORS * CYCLES;
const HALF_PHASES = 2 * CYCLES + 1;
const SHIFT = 1 / 2;

const base36CharArray = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
];

const setSeed = (e) => {
  let seed = cyrb53(e);
  randy.setState({ seed: Array.from(Array(32)).map(() => seed), idx: 1 });
};

const DNA = ({ width, height, cipherKey }) => {
  const size = { width, height };

  const [text, setText] = useState("");
  const [start, setStart] = useState(false);
  const [calculating, setCalculating] = useState(false);
  const [action, setAction] = useState("decrypt");
  const [animatedOutput, setAnimatedOutput] = useState([""]);

  const canvasRef = useRef(null);
  const requestIdRef = useRef(null);

  const dnaRef = useRef({ done: true });

  const generateColor = () => `hsl(${randy.randInt(0, 360)}, 100%, 50%)`;

  const getYConnectorPlacements = () => {
    // TODO: replace with a modulo
    const firstIntersection = (size.height / HALF_PHASES) * SHIFT;
    const divisor = (CONNECTORS + 1) * HALF_PHASES;
    const yIntersections = Array.from(
      { length: HALF_PHASES - 1 },
      (_, i) => (size.height * i) / HALF_PHASES + firstIntersection
    );
    return [].concat(
      ...yIntersections.map((e) =>
        Array.from(
          { length: CONNECTORS },
          (_, i) => e + (size.height * (i + 1)) / divisor
        )
      )
    );
  };

  const calcX = (y, phase = 0) =>
    size.width / 2 +
    (size.width / 4) *
      Math.sin(phase + (y * HALF_PHASES * Math.PI) / (size.height - 1));

  const getOutput = () => {
    try {
      return action === "decrypt"
        ? decrypt(text, cipherKey)
        : encrypt(text, cipherKey);
    } catch (error) {
      return false;
    }
  };

  const computeDNA = () => {
    const dna = dnaRef.current;

    if (dna.error) {
      dna.structure.y += 1;
      return;
    }

    setCalculating(true);

    if (dna.structure.draw) {
      if (dna.structure.yConnectorPlacements.some((e) => e < dna.structure.y)) {
        dna.structure.draw = false; // pause drawing the structure
        dna.connector.draw = true; // begin drawing the connector

        dna.connector.x = Math.max(dna.structure.xl, dna.structure.xr);
        dna.connector.dx = 0;
        dna.connector.color = randy.choice(dna.connector.colors);
        dna.connector.swappedColor = false;
        return;
      }

      dna.structure.lastXl = dna.structure.xl;
      dna.structure.lastXr = dna.structure.xr;

      dna.structure.xl = calcX(dna.structure.y, Math.PI * SHIFT);
      dna.structure.xr = calcX(dna.structure.y, Math.PI * (SHIFT - 1));

      dna.structure.y += dna.structure.dy;

      if (dna.structure.y > size.height) {
        dna.structure.draw = false;
        dna.connector.draw = false;
      }
    } else if (dna.connector.draw) {
      let minX = Math.min(dna.structure.xl, dna.structure.xr);

      if (dna.connector.x < minX) {
        dna.output.connectorsDrawn += 1;

        if (
          dna.output.connectorsDrawn %
            Math.ceil(TOTAL_CONNECTORS / dna.output.value.length) ===
          0
        ) {
          let nonReplacedIndices = Array.from(
            { length: dna.output.chunkedValue.length },
            (_, i) => i
          ).filter((e) => !dna.output.indicesToReplace.includes(e));
          dna.output.indicesToReplace = [
            ...dna.output.indicesToReplace,
            randy.choice(nonReplacedIndices),
          ];
        }

        dna.structure.yConnectorPlacements.shift();

        dna.connector.draw = false; // stop drawing the connector
        dna.structure.draw = true; // resume drawing the structure

        return;
      }

      dna.connector.dx = Math.min(
        dna.connector.maxDx,
        Math.abs(minX - dna.connector.x) + 0.5
      );
      dna.connector.x -= dna.connector.dx;

      if (dna.connector.x < size.width / 2 - 1 && !dna.connector.swappedColor) {
        dna.connector.color = randy.choice(dna.connector.colors);
        dna.connector.swappedColor = true;
      }
    }
    if (dna.structure.draw || dna.connector.draw) {
      // Create random array of base36 chars and replace parts of that array with the real text when each connector is completed

      setAnimatedOutput(
        Array.from({ length: dna.output.value.length }, () =>
          randy.choice(base36CharArray)
        )
          .nchunks(TOTAL_CONNECTORS)
          .map((e, i) =>
            dna.output.indicesToReplace.includes(i)
              ? dna.output.chunkedValue[i]
              : e
          )
      );
    } else if (!dna.structure.draw && !dna.connector.draw) {
      setAnimatedOutput(dna.output.value);
      setCalculating(false);
      console.log("finishing");
      console.log(dna.calculating);
      dna.done = true;
    }
  };

  useEffect(() => {
    if (start) {
      beginAnimation();
    } else if (!start) {
      resetAnimation();
    }
  }, [start]);

  useEffect(() => {
    setStart(() => false);
    resetAnimation();
  }, [cipherKey, action, text]);

  const beginAnimation = () => {
    resetAnimation();

    dnaRef.current.done = false;

    requestIdRef.current = requestAnimationFrame(tick);
  };

  const resetAnimation = () => {
    const ctx = canvasRef.current.getContext("2d");

    ctx.clearRect(0, 0, size.width, size.height);

    dnaRef.current.done = true;

    setSeed(text);

    const structure = {
      draw: true,
      y: 0,
      dy: 1,
      xl: calcX(0, Math.PI * SHIFT),
      xr: calcX(0, Math.PI * (SHIFT - 1)),
      lastXl: 0,
      lastXr: 0,
      colors: [generateColor(), generateColor()],
      yConnectorPlacements: getYConnectorPlacements(),
    };

    const connector = {
      draw: false,
      x: 0,
      dx: 0,
      maxDx: 5,
      color: "",
      swappedColor: false,
      colors: [
        generateColor(),
        generateColor(),
        generateColor(),
        generateColor(),
      ],
    };

    const value = getOutput() || undefined;

    const output = {
      value,
      chunkedValue: value
        ? value.split("").nchunks(TOTAL_CONNECTORS)
        : undefined,
      indicesToReplace: [],
      connectorsDrawn: 0,
    };

    dnaRef.current = {
      ...dnaRef.current,
      error: !getOutput(),
      structure: structuredClone(structure),
      connector: structuredClone(connector),
      output: structuredClone(output),
    };

    setAnimatedOutput([""]);

    setSeed(cipherKey);
  };

  const renderFrame = () => {
    computeDNA();

    const ctx = canvasRef.current.getContext("2d");
    frameRenderer.call(ctx, size, dnaRef.current);
  };

  const tick = () => {
    if (!canvasRef.current) return;

    if (!dnaRef.current.done) {
      renderFrame();
      requestIdRef.current = requestAnimationFrame(tick);
    } else {
      setCalculating(false);
      cancelAnimationFrame(requestIdRef.current);
    }
  };

  return (
    <DNAContainer>
      <DNAAudioManager
        calculating={calculating}
        cipherKey={cipherKey}
        start={start}
        reverse={action === "decrypt" ? 0 : 1}
      />
      <DNAInput onChange={(e) => setText(e.target.value)} maxLength={40} />
      <DNACipherKey>{cipherKey}</DNACipherKey>
      <DNACanvasContainer>
        <ReverseSwitch
          value={action === "decrypt" ? 1 : 0}
          changeValue={() =>
            setAction((e) => (e === "decrypt" ? "encrypt" : "decrypt"))
          }
        />
        <DNACanvas {...size} ref={canvasRef} start={start ? 1 : 0} />
        <StartSwitch
          value={start ? 1 : 0}
          changeValue={() => setStart((e) => !e)}
        />
      </DNACanvasContainer>
      <DNAHazardLine />
      <DNAOutput>{animatedOutput}</DNAOutput>
    </DNAContainer>
  );
};

export default DNA;
