import React, { useState } from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import Selectors from "components/Selectors.jsx";
import DNA from "components/DNA.jsx";

const App = () => {
  const [cipherKey, setCipherKey] = useState(
    Array.from(
      { length: 4 },
      () =>
        "ACGT"[Math.floor(Math.random() * 4)] +
        "ACGT"[Math.floor(Math.random() * 4)]
    )
  );

  return (
    <AppContainer>
      <Selectors initialValue={cipherKey} setKey={setCipherKey} />
      <DNA width="100" height="300" cipherKey={cipherKey.join("")} />
    </AppContainer>
  );
};

export default App;
