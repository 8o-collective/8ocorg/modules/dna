import React, { useState, useEffect } from "react";

import {
  SelectorsContainer,
  GroupsContainer,
} from "assets/styles/Selectors.styles.jsx";

import Group from "components/Controls/Group.jsx";

const Selectors = ({ initialValue, setKey }) => {
  const [portraitDisplay, setPortraitDisplay] = useState(
    window.innerHeight > window.innerWidth
  );

  useEffect(() => {
    const handleResize = () => {
      setPortraitDisplay(window.innerHeight > window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return portraitDisplay ? (
    <SelectorsContainer>
      <GroupsContainer
        style={{ top: "90vh", transform: "translateX(-50%)", left: "50vw" }}
      >
        <Group initialValue={initialValue[0]} setKey={setKey} index={0} />
        <Group initialValue={initialValue[1]} setKey={setKey} index={1} />
        <Group initialValue={initialValue[2]} setKey={setKey} index={2} />
        <Group initialValue={initialValue[3]} setKey={setKey} index={3} />
      </GroupsContainer>
    </SelectorsContainer>
  ) : (
    <SelectorsContainer>
      <GroupsContainer style={{ left: "2vw" }}>
        <Group initialValue={initialValue[0]} setKey={setKey} index={0} />
        <Group initialValue={initialValue[1]} setKey={setKey} index={1} />
      </GroupsContainer>
      <GroupsContainer style={{ right: "2vw" }}>
        <Group initialValue={initialValue[2]} setKey={setKey} index={2} />
        <Group initialValue={initialValue[3]} setKey={setKey} index={3} />
      </GroupsContainer>
    </SelectorsContainer>
  );
};

export default Selectors;
