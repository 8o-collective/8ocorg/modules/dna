import React, { useState, useEffect, useRef } from "react";

/*
Sources:

https://freesound.org/people/LamaMakesMusic/sounds/403556/
https://freesound.org/people/FillSoko/sounds/257958/
https://freesound.org/people/TwistedLemon/sounds/375/
https://freesound.org/people/melack/sounds/13809/
https://freesound.org/people/deleted_user_228014/sounds/420651/
https://freesound.org/people/Kinoton/sounds/351430/
*/

const GroupClick = () => {
  const audio = new Audio(
    require(`../assets/sounds/clicks/group/${Math.floor(
      Math.random() * 4
    )}.ogg`).default
  );

  audio.play();
};

const StartClick = (on) => {
  const audio = new Audio(
    require(`../assets/sounds/clicks/start/${on ? "on" : "off"}_${Math.round(
      Math.random()
    )}.ogg`).default
  );

  audio.play();
};

const ReverseClick = (on) => {
  const audio = new Audio(
    require(`../assets/sounds/clicks/reverse/${on ? "on" : "off"}_${Math.round(
      Math.random()
    )}.ogg`).default
  );

  audio.play();
};

const ClickManager = ({ cipherKey, start, reverse }) => {
  const isMounted = useRef(false);

  useEffect(() => {
    isMounted.current && GroupClick();
  }, [cipherKey]);
  useEffect(() => {
    isMounted.current && StartClick(start);
  }, [start]);
  useEffect(() => {
    isMounted.current && ReverseClick(reverse);
    isMounted.current = true;
  }, [reverse]);

  return null;
};

const DNAAudioManager = ({ calculating, cipherKey, start, reverse }) => {
  const [hum, setHum] = useState(null);
  const [processing, setProcessing] = useState(null);

  useEffect(() => {
    if (start) {
      let audio = new Audio(require(`../assets/sounds/hum/on.ogg`).default);
      audio.loop = true;

      setHum(audio);

      audio.play();
    } else if (!start && hum !== null) {
      hum.pause();
    }
  }, [start]);

  useEffect(() => {
    if (calculating === true && processing === null) {
      let audio = new Audio(
        require(`../assets/sounds/hum/processing.ogg`).default
      );
      audio.loop = true;

      setProcessing(audio);

      audio.play();
    } else if (calculating === false && processing !== null) {
      processing.pause();

      let audio = new Audio(require(`../assets/sounds/hum/end.ogg`).default);
      audio.loop = false;
      audio.play();

      setProcessing(() => null);
    }
  }, [calculating]);

  return <ClickManager cipherKey={cipherKey} reverse={reverse} start={start} />;
};

export { DNAAudioManager };
