import utf8 from "utf8";

const KEY_LENGTH = 8;
const CYCLES = 2;

const DELTA = 0x9e3779b9;
const SUM = DELTA * CYCLES;

Array.prototype.chunk = function (size) {
  return Array.from({ length: Math.ceil(this.length / size) }, (v, i) =>
    this.slice(i * size, i * size + size)
  );
};

Array.prototype.nchunks = function (amount) {
  let subdivisions = Math.max(Math.floor(this.length / amount), 1);

  return this.chunk(subdivisions);
};

const str2vec = (value, l = 4) => {
  let chunks = Array.from(value, (e, i) => value.charCodeAt(i)).chunk(l);

  return chunks.map((chunk) =>
    chunk
      .map((char, i) => char << (8 * i))
      .reduce((partialSum, e) => partialSum + e, 0)
  );
};

const vec2str = (value, l = 4) =>
  Uint8Array.from(
    [].concat(
      ...value.map((e) =>
        Array.from({ length: l }, (x, i) => (e >>> (8 * i)) & 0xff)
      )
    )
  );

const cyrb53 = function (str, seed = 0) {
  let h1 = 0xdeadbeef ^ seed,
    h2 = 0x41c6ce57 ^ seed;
  for (let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i);
    h1 = Math.imul(h1 ^ ch, 2654435761);
    h2 = Math.imul(h2 ^ ch, 1597334677);
  }
  h1 =
    Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^
    Math.imul(h2 ^ (h2 >>> 13), 3266489909);
  h2 =
    Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^
    Math.imul(h1 ^ (h1 >>> 13), 3266489909);
  return 4294967296 * (2097151 & h2) + (h1 >>> 0);
};

Uint8Array.prototype.toBigInt = function () {
  const hex = [];
  const u8 = Uint8Array.from(this);

  u8.forEach(function (i) {
    var h = i.toString(16);
    if (h.length % 2) {
      h = "0" + h;
    }
    hex.push(h);
  });

  return BigInt("0x" + hex.join(""));
};

Uint8Array.prototype.toString = function () {
  return String.fromCharCode.apply(null, new Uint8Array(this));
};

BigInt.prototype.toBuffer = function () {
  let hex = BigInt(this).toString(16);
  if (hex.length % 2) {
    hex = "0" + hex;
  }

  const len = hex.length / 2;
  const u8 = new Uint8Array(len);

  let i = 0;
  let j = 0;
  while (i < len) {
    u8[i] = parseInt(hex.slice(j, j + 2), 16);
    i += 1;
    j += 2;
  }

  return u8;
};

function base36ToBigInt(str) {
  return [...str].reduce(
    (acc, curr) => BigInt(parseInt(curr, 36)) + BigInt(36) * acc,
    0n
  );
}

/**
 * Encrypts text using TEA algorithm.
 *
 * @param   {string} plaintext - String to be encrypted (multi-byte safe).
 * @param   {string} password - Password to be used for encryption (1st 16 chars).
 * @returns {string} Encrypted text (encoded as base64).
 */
const encrypt = (plaintext, password) => {
  plaintext = String(plaintext);
  password = String(password);

  if (plaintext.length == 0) return ""; // nothing to encrypt

  //  v is n-word data vector; converted to array of longs from UTF-8 string
  const v = str2vec(utf8.encode(plaintext));

  //  k is 4-word key; simply convert first 16 chars of password as key
  const k = str2vec(
    utf8.encode(password).slice(0, KEY_LENGTH),
    Math.floor(KEY_LENGTH / 4)
  );

  // compute encodings and concatenate
  const cipherarray = Array.from(v.chunk(2), (e) => vec2str(encipher(e, k)));
  const cipherbytes = new Uint8Array(
    cipherarray.reduce((acc, curr) => [...acc, ...curr], [])
  ).filter((e) => e != 0);

  // convert binary to base34 because we can only convey numbers and lowercase letters in morse
  const ciphertext = cipherbytes.toBigInt().toString(36);

  return ciphertext;
};

/**
 * Decrypts text using TEA algorithm.
 *
 * @param   {string} ciphertext - String to be decrypted.
 * @param   {string} password - Password to be used for decryption (1st 16 chars).
 * @returns {string} Decrypted text.
 * @throws  {Error}  Invalid ciphertext
 */
const decrypt = (ciphertext, password) => {
  ciphertext = String(ciphertext);
  password = String(password);

  if (ciphertext.length == 0) return ""; // nothing to decrypt

  //  v is n-word data vector; converted to array of longs from base64 string
  const v = str2vec(base36ToBigInt(ciphertext).toBuffer().toString());
  //  k is 4-word key; simply convert first 16 chars of password as key
  const k = str2vec(
    utf8.encode(password).slice(0, KEY_LENGTH),
    Math.floor(KEY_LENGTH / 4)
  );

  const plainarray = Array.from(v.chunk(2), (e) => vec2str(decipher(e, k)));
  const plainbytes = new Uint8Array(
    plainarray.reduce((acc, curr) => [...acc, ...curr], [])
  ).filter((e) => e != 0);

  // strip trailing null chars resulting from filling 4-char blocks:
  const plaintext = utf8.decode(plainbytes.toString()).replace(/\0+$/, "");

  return plaintext;
};

/**
 * TEA: enciphers array of unsigned 32-bit integers using 128-bit key.
 *
 * @param   {number[]} v - Data vector.
 * @param   {number[]} k - Key.
 * @returns {number[]} Enciphered vector.
 */
const encipher = (v, k) => {
  if (v.length < 2) v[1] = 0; // algorithm doesn't work for n < 2 so fudge by adding a null
  let [y, z] = v.slice(0, 2);
  let sum = 0;

  for (let i = 0; i < CYCLES; i++) {
    sum += DELTA;
    y += ((z << 4) + k[0]) ^ (z + sum) ^ ((z >>> 5) + k[1]);
    z += ((y << 4) + k[2]) ^ (y + sum) ^ ((y >>> 5) + k[3]);
  }

  return [y, z];
};

/**
 * TEA: deciphers array of unsigned 32-bit integers using 128-bit key.
 *
 * @param   {number[]} v - Data vector.
 * @param   {number[]} k - Key.
 * @returns {number[]} Deciphered vector.
 */
const decipher = (v, k) => {
  if (v.length < 2) v[1] = 0; // algorithm doesn't work for n < 2 so fudge by adding a null
  let [y, z] = v.slice(0, 2);
  let sum = SUM;

  for (let i = 0; i < CYCLES; i++) {
    z -= ((y << 4) + k[2]) ^ (y + sum) ^ ((y >>> 5) + k[3]);
    y -= ((z << 4) + k[0]) ^ (z + sum) ^ ((z >>> 5) + k[1]);
    sum -= DELTA;
  }

  return [y, z];
};

export { encrypt, decrypt, cyrb53, CYCLES };
