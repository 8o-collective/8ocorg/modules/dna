import { CYCLES } from "../actions/dna.cipher.js";

const HALF_PHASES = 2 * CYCLES + 1;
const SHIFT = 1 / 2;
const ERROR_PULSE_TIME = 50;

function frameRenderer(size, dna) {
  const clear = () => this.clearRect(0, 0, size.width, size.height);

  if (dna.error) {
    if ((dna.structure.y / ERROR_PULSE_TIME) % 2 > 1) {
      clear();
      return;
    }

    this.strokeStyle = "#ff0000";
    this.lineWidth = 4;

    const getLine = (y, phase = 1) =>
      size.width / 2 +
      (size.width / 4) *
        Math.sin(phase + (y * HALF_PHASES * Math.PI) / (size.height - 1));

    for (let y = 0; y < size.height; y++) {
      this.beginPath();
      this.moveTo(getLine(y - 1, Math.PI * SHIFT), y);
      this.lineTo(getLine(y, Math.PI * SHIFT), y);
      this.stroke();

      this.beginPath();
      this.moveTo(getLine(y - 1, Math.PI * (SHIFT - 1)), y);
      this.lineTo(getLine(y, Math.PI * (SHIFT - 1)), y);
      this.stroke();

      y += 1;
    }

    return;
  }

  this.lineWidth = 1;

  if (dna.structure.draw) {
    this.beginPath();
    this.strokeStyle = dna.structure.colors[0];
    this.moveTo(dna.structure.lastXl, dna.structure.y - 1);
    this.lineTo(dna.structure.xl, dna.structure.y);
    this.stroke();

    this.beginPath();
    this.strokeStyle = dna.structure.colors[1];
    this.moveTo(dna.structure.lastXr, dna.structure.y - 1);
    this.lineTo(dna.structure.xr, dna.structure.y);
    this.stroke();
  } else if (dna.connector.draw) {
    this.beginPath();
    this.strokeStyle = dna.connector.color;
    this.moveTo(dna.connector.x + dna.connector.dx, dna.structure.y);
    this.lineTo(dna.connector.x, dna.structure.y);
    this.stroke();
  }

  // TODO: draw something when the animation is not started
  // else if (!dna.structure.draw && !dna.connector.draw) {
  // 	for (let x = 0; x < size.width; x++) {
  // 		for (let y = 0; y < size.height; y++) {
  // 			this.fillStyle = `rgba(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255}, 1.0)`;
  // 			this.fillRect( x, y, 1, 1 );
  // 		}
  // 	}
  // }
}

export default frameRenderer;
