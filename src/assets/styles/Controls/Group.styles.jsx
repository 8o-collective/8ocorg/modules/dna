import styled from "styled-components";

const GroupContainer = styled.div`
  display: flex;
  margin: 1vw;
  padding: 2vh max(3vw, 35px);
  gap: 3vw;
  border: 1px solid #222;
  justify-content: center;
  align-items: center;
`;

export { GroupContainer };
