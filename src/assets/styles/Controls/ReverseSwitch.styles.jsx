import styled from "styled-components";

const ReverseSwitchContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ReverseSwitchLabel = styled.div`
  margin: 5px;
  padding: 0px 5px;

  // font-size: 20;

  font-family: ImpactLabel;
  color: white;
  background-color: hsl(0, 50%, 50%);
`;

const ReverseSwitchImage = styled.img`
  z-index: 1;
`;

export { ReverseSwitchContainer, ReverseSwitchLabel, ReverseSwitchImage };
