import styled from "styled-components";

const SliderContainer = styled.div`
  display: flex;
  margin: 0px;
  position: relative;
  top: 0px;
  left: 0px;
`;

const SliderTicks = styled.div`
  position: absolute;
  z-index: 1;
`;

const tickHeight = 26;

const SliderTick = styled.div`
  position: absolute;

  font-size: ${tickHeight * 0.9}px;
  height: ${tickHeight}px;

  font-family: ImpactLabel;
  color: white;
  background-color: hsl(0, 0%, 0%);
`;

const SliderImage = styled.img`
  z-index: 1;
`;

export { SliderContainer, SliderTicks, SliderTick, SliderImage, tickHeight };
