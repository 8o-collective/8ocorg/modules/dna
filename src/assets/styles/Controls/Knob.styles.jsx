import styled from "styled-components";

const KnobContainer = styled.div`
  display: flex;
  margin: 0px;
  position: relative;
  top: 0px;
  left: 0px;
`;

const KnobTicks = styled.div`
  position: absolute;
  z-index: 1;
`;

const KnobTick = styled.div`
  position: absolute;

  font-size: 20px;

  font-family: ImpactLabel;
  color: white;
  background-color: hsl(0, 0%, 0%);
`;

const KnobImage = styled.img`
  z-index: 0;
  image-rendering: pixelated;
`;

export { KnobContainer, KnobTicks, KnobTick, KnobImage };
