import styled from "styled-components";

const DNAContainer = styled.div``;

const DNACanvasContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 50vh;
  left: 50vw;
  transform: translate(-50%, -50%);
`;

const DNACanvas = styled.canvas`
  margin: 0px 3vw;
  border: 1px solid #222;
  image-rendering: pixelated;
`;

const DNAInput = styled.input`
  position: absolute;
  top: 5vh;
  left: 50vw;
  width: 25vw;
  text-align: center;
  transform: translate(-50%, -50%);

  font-family: "IBM3270";

  color: red;
  background-color: inherit;
  border-color: red;
`;

const DNAOutput = styled.div`
  position: absolute;
  top: 92vh;
  left: 50vw;
  text-align: center;
  transform: translate(-50%, -50%);
  padding: 5px;

  font-family: "IBM3270";

  color: white;
  ${(props) => props.children.length !== 1 && `border: 1px solid #222;`}
`;

const DNACipherKey = styled.div`
  position: absolute;
  top: 15vh;
  left: 50vw;
  text-align: center;
  transform: translate(-50%, -50%);
  padding: 5px;

  font-family: "IBM3270";

  color: white;
  border: 1px solid #222;
`;

const DNAHazardLine = styled.div`
  position: absolute;
  top: 85vh;
  left: 50vw;
  transform: translate(-50%, -50%);

  height: 0.5vh;
  width: 25vw;
  color: white;
  padding: 5px;
  background-image: repeating-linear-gradient(
    -55deg,
    #000,
    #000 20px,
    #ffb101 20px,
    #ffb101 40px
  );

  @media (max-width: 768px) {
    top: 80vh;
  }
`;

export {
  DNAContainer,
  DNACanvasContainer,
  DNACanvas,
  DNAInput,
  DNAOutput,
  DNACipherKey,
  DNAHazardLine,
};
