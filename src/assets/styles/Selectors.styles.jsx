import styled from "styled-components";

const SelectorsContainer = styled.div``;

const GroupsContainer = styled.div`
  display: flex;
  flex-direction: column;

  position: absolute;
  top: 50vh;
  transform: translateY(-50%);
`;

export { SelectorsContainer, GroupsContainer };
